class Api::TenantsController < ApplicationController 
  before_action :set_levels, only: [:index]
  def index
    @tenants = Tenant.active
  end

  private 
  
  def set_levels
    @levels = Level.all
  end
end