class Level < ApplicationRecord
  has_many :tenant_levels
  has_many :tenants, through: :tenant_levels
end
