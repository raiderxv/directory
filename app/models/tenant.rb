class Tenant < ApplicationRecord
  belongs_to :level

  has_one_attached :photo

  scope :active, -> {where is_active: true}

  delegate :name, to: :level, prefix: :level
end