class TenantLevel < ApplicationRecord
  belongs_to :tenant
  belongs_to :level
end
