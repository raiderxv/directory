json.extract! tenant, :id, :name, :description, :unit_no, :level_id, :level_name
json.photo_url url_for(tenant.photo) if tenant.photo.attached?