json.data do
  json.array! @tenants, partial: 'tenant', as: :tenant
end
json.success true