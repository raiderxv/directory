Rails.application.routes.draw do
  resources :levels
  resources :tenants 
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  # root "articles#index"
  namespace :api do 
    resources :tenants
    resources :levels 
  end
end
