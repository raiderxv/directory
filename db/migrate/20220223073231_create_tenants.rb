class CreateTenants < ActiveRecord::Migration[7.0]
  def change
    create_table :tenants do |t|
      t.string :name, null: false
      t.text :description, null: true
      t.boolean :is_active, null: false, default: true
      t.string :unit_no, null: false
      t.timestamps
    end
  end
end
