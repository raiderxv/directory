class CreateTenantLevels < ActiveRecord::Migration[7.0]
  def change
    create_table :tenant_levels do |t|
      t.belongs_to :tenant, null: false, foreign_key: true
      t.belongs_to :level, null: false, foreign_key: true

      t.timestamps
    end
  end
end
