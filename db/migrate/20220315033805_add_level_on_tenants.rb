class AddLevelOnTenants < ActiveRecord::Migration[7.0]
  def change
    add_belongs_to :tenants, :level, after: :unit_no
  end
end
